<?php
//Get user credentials to access the page
$realm = 'Restricted area';

//user => password
$users = array($_SERVER['USERNAME'] => $_SERVER['PASSWORD']);


if (empty($_SERVER['PHP_AUTH_DIGEST'])) {
    header('HTTP/1.1 401 Unauthorized');
    header('WWW-Authenticate: Digest realm="'.$realm.
           '",qop="auth",nonce="'.uniqid().'",opaque="'.md5($realm).'"');

    die('You must login to access this page!');
}


// analyze the PHP_AUTH_DIGEST variable
if (!($data = http_digest_parse($_SERVER['PHP_AUTH_DIGEST'])) ||
    !isset($users[$data['username']]))
    die('Wrong Credentials!');


// generate the valid response
$A1 = md5($data['username'] . ':' . $realm . ':' . $users[$data['username']]);
$A2 = md5($_SERVER['REQUEST_METHOD'].':'.$data['uri']);
$valid_response = md5($A1.':'.$data['nonce'].':'.$data['nc'].':'.$data['cnonce'].':'.$data['qop'].':'.$A2);

if ($data['response'] != $valid_response)
    die('Wrong Credentials!');

// ok, valid username & password
echo 'You are logged in as: ' . $data['username'];


// function to parse the http auth header
function http_digest_parse($txt)
{
    // protect against missing data
    $needed_parts = array('nonce'=>1, 'nc'=>1, 'cnonce'=>1, 'qop'=>1, 'username'=>1, 'uri'=>1, 'response'=>1);
    $data = array();
    $keys = implode('|', array_keys($needed_parts));

    preg_match_all('@(' . $keys . ')=(?:([\'"])([^\2]+?)\2|([^\s,]+))@', $txt, $matches, PREG_SET_ORDER);

    foreach ($matches as $m) {
        $data[$m[1]] = $m[3] ? $m[3] : $m[4];
        unset($needed_parts[$m[1]]);
    }

    return $needed_parts ? false : $data;
}
?>

<!DOCTYPE html>
    <html lang="en">
    <head>
        <link href="css/adminstyle.css" rel="stylesheet" type="text/css" />
        <script src="https://unpkg.com/vue@next"></script>
    </head>

    <body>
    <?php
    require 'vendor/autoload.php';

    //Update ts and/or database on form submit
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        //If denied, remove form from db
        if ($_POST["action"] == "deny") {
            $db = pg_connect($_SERVER['DATABASE_URL']) or die('Could not connect to database ' . $_SERVER['DATABASE_URL']);

            $query = "DELETE FROM forms WHERE id = " . $_POST["id"] . ";";
            $result = pg_query($query);
            if (!$result) {
                echo "Could not access table";
            }
            pg_free_result($result);

            $query = "DELETE FROM form_images WHERE form_id = " . $_POST["id"] . ";";
            $result = pg_query($query);
            if (!$result) {
                echo "Could not access table";
            }
            pg_free_result($result);

            pg_close($db);

            echo "<script>alert('Denied Form Submission!');</script>";
        //If accepted update ts, move image to volume, and remove form from db
        } else {
            //Read and load graph from fuseki ts
            $ts = new \EasyRdf\GraphStore("https://fuseki.lincsproject.ca/clode");
            $graph = $ts->getDefault();

            // Translate form data into graph nodes
            $submitter = $graph->newBnode("lincs:person");
            $submitter->set("lincs:firstName", $_POST["firstName"]);
            $submitter->set("lincs:lastName", $_POST["lastName"]);
            if (trim($_POST["altNames"]) != "") {
                $submitter->set("lincs:altNames", $_POST["altNames"]);
            }
            $submitter->set("lincs:orcidURL", $_POST["orcidURL"]);
            $submitter->set("lincs:vizLink", $_POST["vizLink"]);

            $type = "event";
            for ($i=0; $i<2; $i++) {
                if (!empty($_POST[$type . "Name"])) {
                    $unkownCount = 0;
                    $keyCount = 0;
                    $orgCount = 0;
                    $orgRoleCount = 0;
                    for ($count = 0; $count < count($_POST[$type . "Name"]); $count++) {
                        if ($_POST[$type . "Type"][$count] == "unknown") {
                            $info = $graph->newBnode("lincs:".$type);
                            $info->set("lincs:name", $_POST[$type . "Name"][$count]);
                            
                            if (!empty($info->get("lincs:has".str_replace(' ', '_', $_POST[$type . "Role"][$count])))) {
                                $info->add("lincs:has".str_replace(' ', '_', $_POST[$type . "Role"][$count]), $submitter);
                            } else {
                                $info->set("lincs:has".str_replace(' ', '_', $_POST[$type . "Role"][$count]), $submitter);
                            }
    
                            if (!empty($submitter->get("lincs:is".str_replace(' ', '_', $_POST[$type . "Role"][$count])))) {
                                $submitter->add("lincs:is".str_replace(' ', '_', $_POST[$type . "Role"][$count]), $info);
                            } else {
                                $submitter->set("lincs:is".str_replace(' ', '_', $_POST[$type . "Role"][$count]), $info);
                            }

                            $info->set("lincs:description", $_POST[$type . "Desc"][$unkownCount]);
    
                            $keyCount += 1;
                            while(!empty($_POST[$type . "Keywords"][$keyCount]) && $_POST[$type . "Keywords"][$keyCount] != "") {
                                if (!empty($info->get("lincs:hasKeywords"))) {
                                    $info->add("lincs:hasKeywords", $_POST[$type . "Keywords"][$keyCount]);
                                } else {
                                    $info->set("lincs:hasKeywords", $_POST[$type . "Keywords"][$keyCount]);
                                }
                                $keyCount += 1;
                            }
    
                            $info->set("lincs:firstYear", $_POST[$type . "FirstYear"][$unkownCount]);
                            $info->set("lincs:lastYear", $_POST[$type . "LastYear"][$unkownCount]);
                            $info->set("lincs:website", $_POST[$type . "Website"][$unkownCount]);
                            $info->set("lincs:image", $_POST[$type . "Image"][$unkownCount]);
                            if (!filter_var($_POST[$type . "Image"][$unkownCount], FILTER_VALIDATE_URL)) { 
                                rename('data/formUploads/' . $_POST[$type . "Image"][$unkownCount], 'data/bannerImages/' . $_POST[$type . "Image"][$unkownCount]);
                            }
    
                            $orgCount += 1;
                            while(!empty($_POST[$type . "Projects"][$orgCount]) && $_POST[$type . "Projects"][$orgCount] != "") {
                                if (!empty($info->get("lincs:has".str_replace(' ', '_', $_POST[$type . "ProjectsRole"][$orgRoleCount])."Org"))) {
                                    $info->add("lincs:has".str_replace(' ', '_', $_POST[$type . "ProjectsRole"][$orgRoleCount])."Org", $_POST[$type . "Projects"][$orgCount]);
                                } else {
                                    $info->set("lincs:has".str_replace(' ', '_', $_POST[$type . "ProjectsRole"][$orgRoleCount])."Org", $_POST[$type . "Projects"][$orgCount]);
                                }
                                
                                $orgCount += 1;
                                $orgRoleCount += 1;
                            }
    
                            if (trim($_POST[$type . "ExtraInfo"][$unkownCount]) != "") {
                                $info->set("lincs:extraInfo", $_POST[$type . "ExtraInfo"][$unkownCount]);
                            }
    
                            $unkownCount += 1;
                        } else {
                            $typeArr = $graph->allOfType("lincs:".$type);
                            for ($j = 0; $j < count($typeArr); $j++) {
                                if ($typeArr[$j]->get("lincs:name")->getValue() == $_POST[$type . "Name"][$count]) {
                                    if (!empty($typeArr[$j]->get("lincs:has".str_replace(' ', '_', $_POST[$type . "Role"][$count])))) {
                                        $typeArr[$j]->add("lincs:has".str_replace(' ', '_', $_POST[$type . "Role"][$count]), $submitter);
                                    } else {
                                        $typeArr[$j]->set("lincs:has".str_replace(' ', '_', $_POST[$type . "Role"][$count]), $submitter);
                                    }

                                    if (!empty($submitter->get("lincs:is".str_replace(' ', '_', $_POST[$type . "Role"][$count])))) {
                                        $submitter->add("lincs:is".str_replace(' ', '_', $_POST[$type . "Role"][$count]), $typeArr[$j]);
                                    } else {
                                        $submitter->set("lincs:is".str_replace(' ', '_', $_POST[$type . "Role"][$count]), $typeArr[$j]);
                                    }

                                    break;
                                }
                            }
                        }
                    }
                }

                $type = "participation";
            }

            if (!empty($_POST["publicationName"])) {
                $keyCount = 0;
                $orgCount = 0;
                $orgRoleCount = 0;
                for ($count = 0; $count < count($_POST["publicationName"]); $count++) {
                    if ($_POST["publicationType"][$count] == "unknown") {
                        $info = $graph->newBnode("lincs:publication");
                        $info->set("lincs:name", $_POST["publicationName"][$count]);
                        $info->set("lincs:has".str_replace(' ', '_', $_POST["publicationRole"][$count]), $submitter);
                        $submitter->set("lincs:is".str_replace(' ', '_', $_POST["publicationRole"][$count]), $info);
                        if (trim($_POST["publicationDoi"][$count]) != "") {
                            $info->set("lincs:doi", $_POST["publicationDoi"][$count]);
                        }

                        $keyCount += 1;
                        while(!empty($_POST["publicationKeywords"][$keyCount]) && $_POST["publicationKeywords"][$keyCount] != "") {
                            if (!empty($info->get("lincs:hasKeywords"))) {
                                $info->add("lincs:hasKeywords", $_POST["publicationKeywords"][$keyCount]);
                            } else {
                                $info->set("lincs:hasKeywords", $_POST["publicationKeywords"][$keyCount]);
                            }
                            $keyCount += 1;
                        }

                        $orgCount += 1;
                        while(!empty($_POST["publicationProjects"][$orgCount]) && $_POST["publicationProjects"][$orgCount] != "") {
                            $orgTitle = str_replace(' ', '_', $_POST["publicationProjectsRole"][$orgRoleCount]);
                            if (!empty($info->get("lincs:has".str_replace('/', '_', $orgTitle)."Org"))) {
                                $info->add("lincs:has".str_replace('/', '_', $orgTitle)."Org", $_POST["publicationProjects"][$orgCount]);
                            } else {
                                $info->set("lincs:has".str_replace('/', '_', $orgTitle)."Org", $_POST["publicationProjects"][$orgCount]);
                            }
                                
                            $orgCount += 1;
                            $orgRoleCount += 1;
                        }

                        $info->set("lincs:authors", $_POST["publicationAuthors"][$count]);
                        $info->set("lincs:date", $_POST["publicationDate"][$count]);
                        $info->set("lincs:year", $_POST["publicationYear"][$count]);
                        $info->set("lincs:website", $_POST["publicationLink"][$count]);
                        if (trim($_POST["publicationReference"][$count]) != "") {
                            $info->set("lincs:reference", $_POST["publicationReference"][$count]);
                        }
                        if (trim($_POST["publicationExtraInfo"][$count]) != "") {
                            $info->set("lincs:extraInfo", $_POST["publicationExtraInfo"][$count]);
                        }
                    } else {
                        $typeArr = $graph->allOfType("lincs:publication");
                        for ($j = 0; $j < count($typeArr); $j++) {
                            if ($typeArr[$j]->get("lincs:name")->getValue() == $_POST["publicationName"][$count]) {
                                if (!empty($typeArr[$j]->get("lincs:has".str_replace(' ', '_', $_POST["publicationRole"][$count])))) {
                                    $typeArr[$j]->add("lincs:has".str_replace(' ', '_', $_POST["publicationRole"][$count]), $submitter);
                                } else {
                                    $typeArr[$j]->set("lincs:has".str_replace(' ', '_', $_POST["publicationRole"][$count]), $submitter);
                                }

                                if (!empty($submitter->get("lincs:is".str_replace(' ', '_', $_POST["publicationRole"][$count])))) {
                                    $submitter->add("lincs:is".str_replace(' ', '_', $_POST["publicationRole"][$count]), $typeArr[$j]);
                                } else {
                                    $submitter->set("lincs:is".str_replace(' ', '_', $_POST["publicationRole"][$count]), $typeArr[$j]);
                                }

                                break;
                            }
                        }
                    }
                }
            }

            //Replace the defualt graph in ts to the newly updated graph
            $ts->replaceDefault($graph, "turtle");

            //Remove related entries in db
            $db = pg_connect($_SERVER['DATABASE_URL']) or die('Could not connect to database ' . $_SERVER['DATABASE_URL']);

            $query = "DELETE FROM forms WHERE id = " . $_POST["id"] . ";";
            $result = pg_query($query);
            if (!$result) {
                echo "Could not access table";
            }
            pg_free_result($result);

            $query = "DELETE FROM form_images WHERE form_id = " . $_POST["id"] . ";";
            $result = pg_query($query);
            if (!$result) {
                echo "Could not access table";
            }
            pg_free_result($result);
            
            pg_close($db);

            echo "<script>alert('Accepted Form Submission!');</script>";
        }
    }
    ?>

        <h1>Form Submissions</h1>
        <div id="forms">
            <?php
            // Insert a form dropdown for each entry in the db
            $db = pg_connect($_SERVER['DATABASE_URL']) or die('Could not connect to database ' . $_SERVER['DATABASE_URL']);

            $query = "SELECT * FROM forms ORDER BY id";
            $result = pg_query($query);
            if (!$result) {
                echo "Could not access table";
            }

            while($row = pg_fetch_row($result)) {
                $query = "SELECT * FROM form_images WHERE form_id = " . $row[0] . " ORDER BY id";
                $resultImg = pg_query($query);
                if (!$resultImg) {
                    echo "Could not access table";
                }

                // Handle image uploads by updating filename and moving db images into uploads folder on the volume for easy previewing
                $formInfo = json_decode($row[1]);
                if (!empty($formInfo->eventImage)) {
                    for ($i = 0; $i < count($formInfo->eventImage); $i++) {
                        if ($formInfo->eventImage[$i] == "") {
                            $filename = mb_ereg_replace("([^\w\s\d\-_~,;\[\]\(\).])", '', $formInfo->eventName[$i]);
                            $filename = mb_ereg_replace("([\.]{2,})", '', $filename);
                            $filename = preg_replace('/[.]/', '', $filename);
                            $filename .= $row[0] . $i . ".png";

                            $formInfo->eventImage[$i] = $filename;
                            if (!file_exists("data/formUploads/" . $filename)) {
                                $imgRes = pg_fetch_row($resultImg);
                                $unes_image = pg_unescape_bytea($imgRes[1]);
                                $img = fopen("data/formUploads/" . $filename, 'wb') or die("cannot open image\n");
                                fwrite($img, $unes_image) or die("cannot write image data\n");
                                fclose($img);
                            }
                        }
                    }
                }

                echo "<form-info id='" . $row[0] . "' info='" . json_encode($formInfo, JSON_HEX_APOS) . "'></form-info>";
            }
            pg_free_result($result);

            pg_close($db);
            ?>
        </div>
        <script src="js/admin.js"></script>
    </body>
</html>
