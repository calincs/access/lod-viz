# LODViz

A visualization of Linked Open Cultural Data in Canada. AKA *Clode*.

The docker file contains an NGINX server with PHP enabled to serve this project.

This version of the Linked Lists tool emerges from a collaboration between the [UCLAB](https://uclab.fh-potsdam.de/) and [LINCS](https://lincsproject.ca), which will extend the [code](https://github.com/nrchtct/linkedlists_lod3) to work with linked data.

Currently deploys to [http://ecosystem.lincsproject.ca/](http://ecosystem.lincsproject.ca/)
