import csv
from rdflib import Graph, Literal, BNode, Namespace
from rdflib.namespace import RDF

g = Graph()
LINCS = Namespace("lincs:")
g.bind("ns0", LINCS)

with open('data.csv', newline='') as file:
    reader = csv.reader(file)
    data = list(reader)

#print(data[0])
data.pop(0)

personNodes = {}
objectNodes = {}
for row in data:
    affiliated = row[6].split(";")
    first = True
    for person in affiliated:
        if personNodes.get(person) is None:
            name = person.split(" ", 1)
            if name != None:
                personNode = BNode()
                g.add((personNode, RDF.type, LINCS.person))
                g.add((personNode, LINCS.firstName, Literal(name[0])))
                if len(name) > 1:
                    g.add((personNode, LINCS.lastName, Literal(name[1])))
                personNodes[person] = personNode
        else:
            personNode = personNodes.get(person)

        if objectNodes.get(row[1]) is None:
            entry = BNode()

            if "project" in row[0][:-1].lower():
                g.add((entry, RDF.type, LINCS["participation"]))
            else:
                g.add((entry, RDF.type, LINCS[row[0][:-1].lower()]))

            g.add((entry, LINCS.name, Literal(row[1])))
            g.add((personNode, LINCS.isOther, entry))
            g.add((entry, LINCS.hasOther, personNode))
            keywords = row[5].split(";")
            for keyword in keywords:
                g.add((entry, LINCS.hasKeywords, Literal(keyword)))
            g.add((entry, LINCS.firstYear, Literal(row[12])))
            g.add((entry, LINCS.lastYear, Literal(row[13])))
            g.add((entry, LINCS.website, Literal(row[3])))
            orgs = row[10].split(";")
            for org in orgs:
                g.add((entry, LINCS.hasOtherOrg, Literal(org)))

            objectNodes[row[1]] = entry
            if row[0][:-1].lower() == "publication":
                g.add((entry, LINCS.authors, Literal(row[6].replace(";", ", "))))
            else: 
                g.add((entry, LINCS.image, Literal(row[8])))
                g.add((entry, LINCS.description, Literal(row[4].split("<br/>")[0])))
        else:
            entry = objectNodes.get(row[1])
            g.add((personNode, LINCS.isOther, entry))
            g.add((entry, LINCS.hasOther, personNode))

print(g.serialize(format="turtle"))