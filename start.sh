#!/usr/bin/env bash

# move extra jar files from the home folder to the mounted data folder
mkdir -p /data/bannerImages
mkdir -p /data/formUploads
cp -nr /app/bannerImages/* /data/bannerImages/

# kick off the upstream entrypoint:
echo "upstream command: $@"
exec "$@"

# don't exit the container
#exec tail -f /dev/null