<!DOCTYPE html>
    <html lang="en">
    <head>
        <link href="css/formstyle.css" rel="stylesheet" type="text/css" />
        <script src="https://unpkg.com/vue@next"></script>
    </head>

    <body>
        <?php
        require 'vendor/autoload.php';

        //Process and submit form to database on POST
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $db = pg_connect($_SERVER['DATABASE_URL']) or die('Could not connect to database ' . $_SERVER['DATABASE_URL']);
            $table = "CREATE TABLE IF NOT EXISTS forms (" .
                "id INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY," .
                "form_info json)";
            $tableResult = pg_query($table);
            if (!$tableResult) {
                echo "Could not access table";
            }
            pg_free_result($tableResult);
            $table = "CREATE TABLE IF NOT EXISTS form_images (" .
                "id INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY," .
                "img BYTEA," .
                "form_id INT)";
            $tableResult = pg_query($table);
            if (!$tableResult) {
                echo "Could not access table";
            }
            pg_free_result($tableResult);

            $type = "event";
            for ($i = 0; $i < 2; $i++) {
                if (!empty($_POST[$type . "Image"])) {
                    for ($j = 0; $j < count($_POST[$type . "Image"]); $j++) {
                        if (!empty($_FILES[$type . "ImageUpload"]["tmp_name"][$j])) {
                            $check = getimagesize($_FILES[$type . "ImageUpload"]["tmp_name"][$j]);
                            if ($check !== false) { //Update image name to prevent overlapping
                                $img = fopen($_FILES[$type . "ImageUpload"]["tmp_name"][$j], 'r') or die("cannot read image\n");
                                $data = fread($img, filesize($_FILES[$type . "ImageUpload"]["tmp_name"][$j]));
                                $es_data = pg_escape_bytea($data);
                                $result = pg_exec($db, "INSERT INTO form_images(img, form_id) VALUES('$es_data', ((SELECT COALESCE(MAX(id), 0) FROM forms) + 1));");
                                pg_free_result($result);

                                $_POST[$type . "Image"][$j] = "";
                            }
                        }
                    }
                }
                $type = "participation";
            }

            $saveInfo = json_encode($_POST, JSON_HEX_APOS);
            $query = "INSERT INTO forms (form_info) " .
                "VALUES ('" . $saveInfo . "')";
            $result = pg_query($query);
            if (!$result) {
                echo "Update failed";
            }
            pg_free_result($result);

            pg_close($db);
        }

        function test_input($data) {
            $data = trim($data);
            $data = stripslashes($data);
            $data = htmlspecialchars($data);
            return $data;
        }

        //Read and load graph from fuseki ts
        $graph = new \EasyRdf\Graph("https://fuseki.lincsproject.ca/clode");
        $graph->load();

        $orgArr = Array();

        //Get info from graph to fill autocomplete sections in the form
        $eventArr = $graph->allOfType("lincs:event");
        $eventNames = Array();
        for ($i=0; $i < count($eventArr); $i++) {
            $eventNames[$i] = $eventArr[$i]->get("lincs:name")->getValue();

            $orgArr = array_merge($orgArr, $eventArr[$i]->all("lincs:hasSponsorOrg"));
            $orgArr = array_merge($orgArr, $eventArr[$i]->all("lincs:hasHostOrg"));
            $orgArr = array_merge($orgArr, $eventArr[$i]->all("lincs:hasOtherOrg"));
        }
        $events = implode(";", $eventNames);
       
        $projectArr = $graph->allOfType("lincs:participation");
        $projectNames = Array();
        for ($i=0; $i < count($projectArr); $i++) {
            $projectNames[$i] = $projectArr[$i]->get("lincs:name")->getValue();

            $orgArr = array_merge($orgArr, $projectArr[$i]->all("lincs:hasSponsorOrg"));
            $orgArr = array_merge($orgArr, $projectArr[$i]->all("lincs:hasHostOrg"));
            $orgArr = array_merge($orgArr, $projectArr[$i]->all("lincs:hasOtherOrg"));
        }
        $projects = implode(";", $projectNames);

        $publicationArr = $graph->allOfType("lincs:publication");
        $publicationNames = Array();
        for ($i=0; $i < count($publicationArr); $i++) {
            $publicationNames[$i] = $publicationArr[$i]->get("lincs:name")->getValue();

            $orgArr = array_merge($orgArr, $publicationArr[$i]->all("lincs:hasSponsorOrg"));
            $orgArr = array_merge($orgArr, $publicationArr[$i]->all("lincs:hasInstitutional_AffiliationOrg"));
            $orgArr = array_merge($orgArr, $publicationArr[$i]->all("lincs:hasResearch_PartnerOrg"));
            $orgArr = array_merge($orgArr, $publicationArr[$i]->all("lincs:hasPublishing_Dissemination_PartnerOrg"));
        }
        $publications = implode(";", $publicationNames);

        $orgArr = array_merge($orgArr, $projectNames);
        $orgArr = array_unique($orgArr);
        $orgs = implode(";", $orgArr);
        ?>

        <!--Displays input form on GET-->
        <?php if($_SERVER["REQUEST_METHOD"] != "POST"): ?>
            <div id="form">
                <div id="form-info">
                    <span id="eventsInfo" hidden><?php echo $events; ?></span>
                    <span id="projectsInfo" hidden><?php echo $projects; ?></span>
                    <span id="publicationsInfo" hidden><?php echo $publications; ?></span>
                    <span id="orgsInfo" hidden><?php echo $orgs; ?></span>
                    <h1>Linking Open Cultural Data in Canada Data Intake</h1>
                    <p>The information we collect here is meant to reflect events, projects, and publications that have contributed to building an ecology for Linked Open Data over the last 5 years or so.</p>
                    <form onsubmit="return validateForm()" autocomplete="off" onkeydown="return event.key != 'Enter';" method="post" enctype="multipart/form-data" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                        <div>
                            <h2>Personal Information</h2>
                            <h3 class="label">Preferred First Name</h3>
                            <input type="text" name="firstName" />

                            <h3 class="label">Preferred Last Name</h3>
                            <input type="text" name="lastName" />

                            <h3 class="label">Alternate Names</h3>
                            <p>Please enter any alternative names that might appear in publications, for multiple names please use a semicolon as a separator.</p>
                            <input type="text" name="altNames" />
                            
                            <h3 class="label">ORCID URL (eg. <a target="_black" href="https://orcid.org/0000-0003-0945-0158">https://orcid.org/0000-0003-0945-0158</a>)</h3>
                            <p>Don’t have an ORCID? You want one! Sign up quickly here: <a target="_black" href="https://orcid.org/register">https://orcid.org/register</a></p>
                            <input type="text" name="orcidURL" />
                            
                            <h3 class="label">Personal home page to link to in visualization</h3>
                            <input type="text" name="vizLink" />
                        </div>
                        <div id="eventInfo">
                            <h2>Event Information</h2>
                            <p>Add all events that you have participated in, either as an attendee, participant, or organizer. Inform us of what roles apply.</p>
                            <div class="autocomplete" style="width:74%;">
                                <input autocomplete="off" id="eventInput" type="search" name="eventInput" v-model="eventName" placeholder="Enter Event Name">
                            </div>
                            <input type="button" value="Add Event" v-on:click="addEvent()" class="beside-input">

                            <event-info v-for="(event, index) in events" :key="index" :eventname="event.name" :infonum="index" :displaytype="event.displaytype"/>
                        </div>
                        <div id="projectParticipation">
                            <h2>Project Participation</h2>
                            <p>In this section, you will be asked to indicate your role in any related projects with which you have been involved.</p>
                            <div class="autocomplete" style="width:74%;">
                                <input autocomplete="off" id="participationInput" type="search" name="participationInput" v-model="participationName" placeholder="Enter Project Name">
                            </div>
                            <input type="button" value="Add Project" v-on:click="addParticipation()" class="beside-input">

                            <participation-info v-for="(participation, index) in participations" :key="index" :participationname="participation.name" :infonum="index" :displaytype="participation.displaytype"/>
                        </div>
                        <div id="publications">
                            <h2>Papers/Publications</h2>
                            <p>Please add a paper/publication that you have taken part in that has taken place in the last 6 years regarding LOD/Diversity/Difference.</p>
                            <div class="autocomplete" style="width:74%;">
                                <input autocomplete="off" id="publicationInput" type="search" name="publicationInput" v-model="publicationName" placeholder="Enter Paper/Publication Title">
                            </div>
                            <input type="button" value="Add Paper/Publication" v-on:click="addPublication()" class="beside-input">

                            <publication-info v-for="(publication, index) in publications" :key="index" :publicationname="publication.name" :infonum="index" :displaytype="publication.displaytype"/>
                        </div>

                        <div id="submit-button">
                            <p>Thank you so much for taking time to enter your information. We hope you’ll enjoy exploring your connection to the Canadian LOD environment through the visualization!</p>
                            <input type="submit" />
                        </div>
                    </form>
                </div>
            </div>
            <script src="js/form.js"></script>
        <!--Displays form submitted on POST--> 
        <?php else: ?>
            <div id="form">
                <div id="form-info">
                <h1>Form Submitted</h1>
                <p>Thank you for agreeing to provide information for the "Linking Open Cultural Data in Canada" visualization being produced by LINCS team.</p>
                <a href="/">→ Return to Home Page</a>
                </div>
            </div>
        <?php endif; ?>
    </body>
</html>
