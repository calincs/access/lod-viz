//Add autocomplete dropdown on text input
function autocomplete(inp, arr, text, _this) {
    var currentFocus;
    inp.addEventListener("input", function(e) {
        var a, b, i, val = this.value;
        closeAllLists();
        if (!val) { return false;}
        currentFocus = -1;
        a = document.createElement("DIV");
        a.setAttribute("id", this.id + "autocomplete-list");
        a.setAttribute("class", "autocomplete-items");
        this.parentNode.appendChild(a);
        for (i = 0; i < arr.length; i++) {
            if (arr[i].toUpperCase().includes(val.toUpperCase())) {
                b = document.createElement("DIV");
                var regExp = new RegExp(`(${val})`, 'ig');
                b.innerHTML = arr[i];
                b.innerHTML = b.innerHTML.replace(regExp, "<strong>$1</strong>");
                b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                b.addEventListener("click", function(e) {
                    if (text == "event") {
                        _this.eventName = this.getElementsByTagName("input")[0].value;
                    } else if (text == "participation") {
                        _this.participationName = this.getElementsByTagName("input")[0].value;
                    } else if (text == "publication") {
                        _this.publicationName = this.getElementsByTagName("input")[0].value;
                    } else if (text == "keyword") {
                        _this.keywordName = this.getElementsByTagName("input")[0].value;
                    } else if (text == "project") {
                        _this.projectName = this.getElementsByTagName("input")[0].value;
                    }
                    closeAllLists();
                });
                a.appendChild(b);
            }
        }
    });
    inp.addEventListener("keydown", function(e) {
        var x = document.getElementById(this.id + "autocomplete-list");
        if (x) x = x.getElementsByTagName("div");
        if (e.keyCode == 40) {
            currentFocus++;
            addActive(x);
        } else if (e.keyCode == 38) {
            currentFocus--;
            addActive(x);
        } else if (e.keyCode == 13) {
            e.preventDefault();
            if (currentFocus > -1) {
                if (x) x[currentFocus].click();
            }
        }
    });
    function addActive(x) {
        if (!x) return false;
        removeActive(x);
        if (currentFocus >= x.length) currentFocus = 0;
        if (currentFocus < 0) currentFocus = (x.length - 1);
        x[currentFocus].classList.add("autocomplete-active");
    }
    function removeActive(x) {
        for (var i = 0; i < x.length; i++) {
            x[i].classList.remove("autocomplete-active");
        }
    }
    function closeAllLists(elmnt) {
        var x = document.getElementsByClassName("autocomplete-items");
        for (var i = 0; i < x.length; i++) {
            if (elmnt != x[i] && elmnt != inp) {
                x[i].parentNode.removeChild(x[i]);
            }
        }
    }
    document.addEventListener("click", function (e) {
        closeAllLists(e.target);
    });
}

//Check for valid url string
function validURL(str) {
    const matchpattern = /^https?:\/\/(?:www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)$/gm;
    return matchpattern.test(_string);
}

//Validate the form input before submission and alert user to issues
function validateForm() {
    var out = true;

    var test = ["firstName", "lastName"];
    for (var i = 0; i < test.length; i++) {
        document.getElementsByName(test[i])[0].value = document.getElementsByName(test[i])[0].value.trim();
        if (document.getElementsByName(test[i])[0].value == "") {
            document.getElementsByName(test[i])[0].style.borderColor = "red";
            document.getElementsByName(test[i])[0].previousSibling.style.color = "red";
            out = false;
        } else {
            document.getElementsByName(test[i])[0].style.borderColor = "black";
            document.getElementsByName(test[i])[0].previousSibling.style.color = "black";
        }
    }

    document.getElementsByName("orcidURL")[0].value = document.getElementsByName("orcidURL")[0].value.trim();
    if (document.getElementsByName("orcidURL")[0].value == "" || !validURL(document.getElementsByName("orcidURL")[0].value)) {
        document.getElementsByName("orcidURL")[0].style.borderColor = "red";
        document.getElementsByName("orcidURL")[0].previousSibling.previousSibling.style.color = "red";
        out = false;
    } else {
        document.getElementsByName("orcidURL")[0].style.borderColor = "black";
        document.getElementsByName("orcidURL")[0].previousSibling.previousSibling.style.color = "black";
    }

    document.getElementsByName("vizLink")[0].value = document.getElementsByName("vizLink")[0].value.trim();
    if (document.getElementsByName("vizLink")[0].value == "" || !validURL(document.getElementsByName("vizLink")[0].value)) {
        document.getElementsByName("vizLink")[0].style.borderColor = "red";
        document.getElementsByName("vizLink")[0].previousSibling.style.color = "red";
        out = false;
    } else {
        document.getElementsByName("vizLink")[0].style.borderColor = "black";
        document.getElementsByName("vizLink")[0].previousSibling.style.color = "black";
    }

    test = ["eventInput", "participationInput", "publicationInput"];
    for (var i = 0; i < test.length; i++) {
        document.querySelectorAll("input[name='" + test[i] + "'][type='search']").forEach(function(element) {
            if (element.value != "") {
                element.style.borderColor = "red";
                element.parentElement.previousSibling.previousSibling.style.color = "red";
                out = false;
            } else {
                element.style.borderColor = "black";
                element.parentElement.previousSibling.previousSibling.style.color = "black";
            }
        });
    }

    test = ["eventDesc[]", "eventFirstYear[]", "eventLastYear[]", "participationDesc[]", "participationFirstYear[]", "participationLastYear[]", "publicationAuthors[]", "publicationDate[]", "publicationYear[]"];
    for (var i = 0; i < test.length; i++) {
        document.querySelectorAll("input[name='" + test[i] + "']").forEach(function(element) {
            if (element.value == "") {
                element.style.borderColor = "red";
                element.previousSibling.style.color = "red";
                out = false;
            } else {
                element.style.borderColor = "black";
                element.previousSibling.style.color = "black";
            }
        });
    }

    test = ["eventWebsite[]", "participationWebsite[]", "publicationLink[]"];
    for (var i = 0; i < test.length; i++) {
        document.querySelectorAll("input[name='" + test[i] + "']").forEach(function(element) {
            if (element.value == "" || !validURL(element.value)) {
                element.style.borderColor = "red";
                element.previousSibling.style.color = "red";
                out = false;
            } else {
                element.style.borderColor = "black";
                element.previousSibling.style.color = "black";
            }
        });
    }

    test = ["eventImage[]", "participationImage[]"];
    for (var i = 0; i < test.length; i++) {
        document.querySelectorAll("input[name='" + test[i] + "']").forEach(function(element) {
            if (element.nextSibling.value == "" && (element.value == "" || !validURL(element.value))) {
                element.style.borderColor = "red";
                element.previousSibling.previousSibling.style.color = "red";
                out = false;
            } else {
                element.style.borderColor = "black";
                element.previousSibling.previousSibling.style.color = "black";
                if (element.nextSibling.value != "") {
                    element.value == "";
                }
            }
        });
    }

    test = ["eventKeywords[]", "eventProjects[]", "participationKeywords[]", "participationProjects[]", "publicationKeywords[]", "publicationProjects[]"];
    for (var i = 0; i < test.length; i++) {
        document.querySelectorAll("input[name='" + test[i] + "'][type='search']").forEach(function(element) {
            if (element.value != "") {
                element.style.borderColor = "red";
                element.parentElement.previousSibling.style.color = "red";
                out = false;
            } else {
                element.style.borderColor = "black";
                element.parentElement.previousSibling.style.color = "black";
            }
        });
    }

    return out;
}

const app = Vue.createApp({
    data() {
        return {
            eventName: "",
            events: [],
            knownEvents: [],
            participationName: "",
            participations: [],
            knownParticipations: [],
            publicationName: "",
            publications: [],
            knownPublications: []
        }
    },



    methods: {
        addEvent() { //Add an event form under the "Events" header
            if (this.eventName != "") {
                if (this.knownEvents.includes(this.eventName)) {
                    this.events.push({"name": this.eventName, "displaytype": "known"});
                } else {
                    this.events.push({"name": this.eventName, "displaytype": "unknown"});
                }
                this.eventName = "";
            }
        },
        removeEvent(index) { //Remove an even form
            this.events.splice(index, 1);
        },
        addParticipation() { //Add a participation form under the "Projects/Participation" header
            if (this.participationName != "") {
                if (this.knownParticipations.includes(this.participationName)) {
                    this.participations.push({"name": this.participationName, "displaytype": "known"});
                } else {
                    this.participations.push({"name": this.participationName, "displaytype": "unknown"});
                }
                this.participationName = "";
            }
        },
        removeParticipation(index) { //Remove a participation form
            this.participations.splice(index, 1);
        },
        addPublication() { //Add a publication form under the "Publications" header
            if (this.publicationName != "") {
                if (this.knownPublications.includes(this.publicationName)) {
                    this.publications.push({"name": this.publicationName, "displaytype": "known"});
                } else {
                    this.publications.push({"name": this.publicationName, "displaytype": "unknown"});
                }
                this.publicationName = "";
            }
        },
        removePublication(index) { //Remove a publication form
            this.publications.splice(index, 1);
        },
    },



    mounted: async function() { //Load autocomplete sections on element load
        this.knownEvents = document.getElementById("eventsInfo").textContent.split(";");
        this.knownParticipations = document.getElementById("projectsInfo").textContent.split(";");
        this.knownPublications = document.getElementById("publicationsInfo").textContent.split(";");

        autocomplete(document.getElementById("eventInput"), this.knownEvents, "event", this);
        autocomplete(document.getElementById("participationInput"), this.knownParticipations, "participation", this);
        autocomplete(document.getElementById("publicationInput"), this.knownPublications, "publication", this);
    }
});

app.component("event-info", { //Event form
    template:
    /*html*/
    `
    <div class="info-card" style="margin-bottom: 15px;">
        <input type="hidden" name="eventType[]" :value="displaytype" />
        <input type="text" name="eventName[]" :value="eventname" style="width: 74%;" readonly>
        <select name="eventRole[]" class="beside-input-exit">
            <option>Organizer</option>
            <option>Participant</option>
            <option>Attendee</option>
            <option>Other</option>
        </select>
        <input type="button" value="X" class="exit-button" v-on:click="remove()"/>

        <div v-if="displaytype == 'unknown'">
            <h3 class="label">Short Description</h3>
            <input type="text" name="eventDesc[]" />

            <h3 class="label">Keywords related to the event</h3>
            <div class="autocomplete" style="width:74%;">
                <input autocomplete="off" :id="'eventKeywords' + infonum" type="search" name="eventKeywords[]" v-model="keywordName" placeholder="Enter Keywords">
            </div>
            <input type="button" value="Add Keyword" v-on:click="addKeyword()" class="beside-input">
            <div v-for="(keyword, index) in keywords">
                <input class="extra-info-card" style="width:37%;" type="text" name="eventKeywords[]" :value="keyword" readonly>
                <input type="button" value="X" class="exit-button" v-on:click="removeKeyword(index)"/>
            </div>

            <h3 class="label">Earliest Year Held</h3>
            <input type="text" name="eventFirstYear[]" />

            <h3 class="label">Latest Year Held</h3>
            <input type="text" name="eventLastYear[]" />

            <h3 class="label">Event Website</h3>
            <input type="text" name="eventWebsite[]" />

            <h3 class="label">Related image</h3>
            <p>If there is a logo or banner related to the project, please share a link <b>or</b> upload an image below.</p>
            <input type="text" name="eventImage[]" />
            <input style="margin-top:8px;" type="file" multiple="multiple" name="eventImageUpload[]">

            <h3 class="label">Associated Organizations/Projects</h3>
            <div class="autocomplete" style="width:74%;">
                <input autocomplete="off" :id="'eventProjects' + infonum" type="search" name="eventProjects[]" v-model="projectName" placeholder="Enter Organization/Project">
            </div>
            <input type="button" value="Add Organization/Project" v-on:click="addProject()" class="beside-input">
            <div v-for="(project, index) in projects">
                <input class="extra-info-card" style="width:37%;" type="text" name="eventProjects[]" :value="project" readonly>
                <select name="eventProjectsRole[]" class="beside-input-small">
                    <option>Sponsor</option>
                    <option>Host</option>
                    <option>Other</option>
                </select>
                <input type="button" value="X" class="exit-button" v-on:click="removeOrg(index)"/>
            </div>

            <h3 class="label">Additional Information</h3>
            <input type="text" name="eventExtraInfo[]" />
        </div>
    </div>
    <br style="padding: 3px;">
    `,


    data() {
        return {
            keywordName: "",
            keywords: [],
            knownKeywords: ["archives/collections", "collaboration", "critical cataloguing", "critical race studies", "cultural heritage", "cultural identities", "data preservation/management", "decolonization", "design", "digital humanities", "digital storytelling", "diversity", "equity", "feminism", "historical change", "inclusiveness", "Indigenous knowledge", "information ethics", "infrastructure", "interfaces", "linked data", "long-form argument", "metadata", "methods documentation", "networks", "ontologies", "open scholarship/access/OER", "pedagogy", "policy", "programming", "publishing", "scholarly editing", "serendipity", "spatial data", "visualization", "vocabularies/authorities"],
            projectName: "",
            projects: [],
            knownProjects: []
        }
    },



    props: {
        eventname: {
            type: String,
            required: true
        },
        infonum: {
            type: String,
            required: true
        },
        displaytype: {
            type: String,
            required: true
        }
    },



    methods: {
        addKeyword() { //Add keyword to form
            if (this.keywordName != "") {
                this.keywords.push(this.keywordName);
                this.keywordName = "";
            }
        },
        addProject() { //Add related project to form
            if (this.projectName != "") {
                this.projects.push(this.projectName);
                this.projectName = "";
            }
        },
        remove() { //Remove this form from the parent form
            this.$parent.removeEvent(parseInt(this.infonum));
        },
        removeKeyword(index) { //Remove a keyword
            this.keywords.splice(index, 1);
        },
        removeOrg(index) { //Remove a related project
            this.projects.splice(index, 1);
        }
    },



    mounted: async function() { //Load autocomplete sections on element load
        if (this.displaytype == "unknown") {
            this.knownProjects = document.getElementById("orgsInfo").textContent.split(";");

            autocomplete(document.getElementById("eventKeywords" + this.infonum), this.knownKeywords, "keyword", this);
            autocomplete(document.getElementById("eventProjects" + this.infonum), this.knownProjects, "project", this);
        }
    }
});

app.component("participation-info", { //Participation form
    template:
    /*html*/
    `
    <div class="info-card" style="margin-bottom: 15px;">
        <input type="hidden" name="participationType[]" :value="displaytype" />
        <input type="text" name="participationName[]" :value="participationname" style="width: 74%;" readonly>
        <select name="participationRole[]" class="beside-input-exit">
            <option>Principal Investigator</option>
            <option>Co-Investigator</option>
            <option>Collaborator</option>
            <option>Member</option>
            <option>RA</option>
            <option>Postdoc</option>
        </select>
        <input type="button" value="X" class="exit-button" v-on:click="remove()"/>

        <div v-if="displaytype == 'unknown'">
            <h3 class="label">Short Description</h3>
            <input type="text" name="participationDesc[]" />

            <h3 class="label">Keywords related to the project</h3>
            <div class="autocomplete" style="width:74%;">
                <input autocomplete="off" :id="'participationKeywords' + infonum" type="search" name="participationKeywords[]" v-model="keywordName" placeholder="Enter Keywords">
            </div>
            <input type="button" value="Add Keyword" v-on:click="addKeyword()" class="beside-input">
            <div v-for="(keyword, index) in keywords">
                <input class="extra-info-card" style="width:37%;" type="text" name="participationKeywords[]" :value="keyword" readonly>
                <input type="button" value="X" class="exit-button" v-on:click="removeKeyword(index)"/>
            </div>

            <h3 class="label">Start Date</h3>
            <input type="text" name="participationFirstYear[]" />

            <h3 class="label">End Date</h3>
            <input type="text" name="participationLastYear[]" />

            <h3 class="label">Project Website</h3>
            <input type="text" name="participationWebsite[]" />

            <h3 class="label">Related image</h3>
            <p>If there is a logo or banner related to the project, please share a link <b>or</b> upload an image below.</p>
            <input type="text" name="participationImage[]" />
            <input style="margin-top:8px;" type="file" multiple="multiple" name="participationImageUpload[]">

            <h3 class="label">Associated Organizations/Projects</h3>
            <div class="autocomplete" style="width:74%;">
                <input autocomplete="off" :id="'participationProjects' + infonum" type="search" name="participationProjects[]" v-model="projectName" placeholder="Enter Organization/Project">
            </div>
            <input type="button" value="Add Organization/Project" v-on:click="addProject()" class="beside-input">
            <div v-for="(project, index) in projects">
                <input class="extra-info-card" style="width:37%;" type="text" name="participationProjects[]" :value="project" readonly>
                <select name="participationProjectsRole[]" class="beside-input-small">
                    <option>Sponsor</option>
                    <option>Host</option>
                    <option>Other</option>
                </select>
                <input type="button" value="X" class="exit-button" v-on:click="removeOrg(index)"/>
            </div>

            <h3 class="label">Additional Information</h3>
            <input type="text" name="participationExtraInfo[]" />
        </div>
    </div>
    <br style="padding: 3px;">
    `,


    data() {
        return {
            keywordName: "",
            keywords: [],
            knownKeywords: ["archives/collections", "collaboration", "critical cataloguing", "critical race studies", "cultural heritage", "cultural identities", "data preservation/management", "decolonization", "design", "digital humanities", "digital storytelling", "diversity", "equity", "feminism", "historical change", "inclusiveness", "Indigenous knowledge", "information ethics", "infrastructure", "interfaces", "linked data", "long-form argument", "metadata", "methods documentation", "networks", "ontologies", "open scholarship/access/OER", "pedagogy", "policy", "programming", "publishing", "scholarly editing", "serendipity", "spatial data", "visualization", "vocabularies/authorities"],
            projectName: "",
            projects: [],
            knownProjects: []
        }
    },



    props: {
        participationname: {
            type: String,
            required: true
        },
        infonum: {
            type: String,
            required: true
        },
        displaytype: {
            type: String,
            required: true
        }
    },



    methods: {
        addKeyword() { //Add keyword to form
            if (this.keywordName != "") {
                this.keywords.push(this.keywordName);
                this.keywordName = "";
            }
        },
        addProject() { //Add related project to form
            if (this.projectName != "") {
                this.projects.push(this.projectName);
                this.projectName = "";
            }
        },
        remove() { //Remove this form from the parent form
            this.$parent.removeParticipation(parseInt(this.infonum));
        },
        removeKeyword(index) { //Remove a keyword
            this.keywords.splice(index, 1);
        },
        removeOrg(index) { //Remove a related project
            this.projects.splice(index, 1);
        }
    },



    mounted: async function() { //Load autocomplete sections on element load
        if (this.displaytype == "unknown") {
            this.knownProjects = document.getElementById("orgsInfo").textContent.split(";");

            autocomplete(document.getElementById("participationKeywords" + this.infonum), this.knownKeywords, "keyword", this);
            autocomplete(document.getElementById("participationProjects" + this.infonum), this.knownProjects, "project", this);
        }
    }
});

app.component("publication-info", { //Publication form
    template:
    /*html*/
    `
    <div class="info-card" style="margin-bottom: 15px;">
        <input type="hidden" name="publicationType[]" :value="displaytype" />
        <input type="text" name="publicationName[]" :value="publicationname" style="width: 74%;" readonly>
        <select name="publicationRole[]" class="beside-input-exit">
            <option>Author</option>
            <option>Editor</option>
            <option>Other</option>
        </select>
        <input type="button" value="X" class="exit-button" v-on:click="remove()"/>

        <div v-if="displaytype == 'unknown'">
            <h3 class="label">DOI (if available)</h3>
            <input type="text" name="publicationDoi[]" />

            <h3 class="label">Keywords related to the paper/publication</h3>
            <div class="autocomplete" style="width:74%;">
                <input autocomplete="off" :id="'publicationKeywords' + infonum" type="search" name="publicationKeywords[]" v-model="keywordName" placeholder="Enter Keywords">
            </div>
            <input type="button" value="Add Keyword" v-on:click="addKeyword()" class="beside-input">
            <div v-for="(keyword, index) in keywords">
                <input class="extra-info-card" style="width:37%;" type="text" name="publicationKeywords[]" :value="keyword" readonly>
                <input type="button" value="X" class="exit-button" v-on:click="removeKeyword(index)"/>
            </div>

            <h3 class="label">Associated Organizations/Projects</h3>
            <div class="autocomplete" style="width:74%;">
                <input autocomplete="off" :id="'publicationProjects' + infonum" type="search" name="publicationProjects[]" v-model="projectName" placeholder="Enter Organization/Project">
            </div>
            <input type="button" value="Add Organization/Project" v-on:click="addProject()" class="beside-input">
            <div v-for="(project, index) in projects">
                <input class="extra-info-card" style="width:37%;" type="text" name="publicationProjects[]" :value="project" readonly>
                <select name="publicationProjectsRole[]" class="beside-input-small">
                    <option>Sponsor</option>
                    <option>Institutional Affiliation</option>
                    <option>Research Partner</option>
                    <option>Publishing/Dissemination Partner</option>
                </select>
                <input type="button" value="X" class="exit-button" v-on:click="removeOrg(index)"/>
            </div>

            <h3 class="label">List all Authors</h3>
            <input type="text" name="publicationAuthors[]" />

            <h3 class="label">Date of paper/publication (If available)</h3>
            <input type="text" name="publicationDate[]" />

            <h3 class="label">Year of paper/publication</h3>
            <input type="text" name="publicationYear[]" />

            <h3 class="label">URL that links to the paper/publication</h3>
            <input type="text" name="publicationLink[]" />

            <h3 class="label">Rest of reference</h3>
            <input type="text" name="publicationReference[]" />

            <h3 class="label">Additional Information</h3>
            <input type="text" name="publicationExtraInfo[]" />
        </div>
    </div>
    <br style="padding: 3px;">
    `,



    data() {
        return {
            keywordName: "",
            keywords: [],
            knownKeywords: ["archives/collections", "collaboration", "critical cataloguing", "critical race studies", "cultural heritage", "cultural identities", "data preservation/management", "decolonization", "design", "digital humanities", "digital storytelling", "diversity", "equity", "feminism", "historical change", "inclusiveness", "Indigenous knowledge", "information ethics", "infrastructure", "interfaces", "linked data", "long-form argument", "metadata", "methods documentation", "networks", "ontologies", "open scholarship/access/OER", "pedagogy", "policy", "programming", "publishing", "scholarly editing", "serendipity", "spatial data", "visualization", "vocabularies/authorities"],
            projectName: "",
            projects: [],
            knownProjects: []
        }
    },



    props: {
        publicationname: {
            type: String,
            required: true
        },
        infonum: {
            type: String,
            required: true
        },
        displaytype: {
            type: String,
            required: true
        }
    },



    methods: {
        addKeyword() { //Add keyword to form
            if (this.keywordName != "") {
                this.keywords.push(this.keywordName);
                this.keywordName = "";
            }
        },
        addProject() { //Add related project to form
            if (this.projectName != "") {
                this.projects.push(this.projectName);
                this.projectName = "";
            }
        },
        remove() { //Remove this form from the parent form
            this.$parent.removePublication(parseInt(this.infonum));
        },
        removeKeyword(index) { //Remove a keyword
            this.keywords.splice(index, 1);
        },
        removeOrg(index) { //Remove a related project
            this.projects.splice(index, 1);
        }
    },



    mounted: async function() { //Load autocomplete sections on element load
        if (this.displaytype == "unknown") {
            this.knownProjects = document.getElementById("orgsInfo").textContent.split(";");

            autocomplete(document.getElementById("publicationKeywords" + this.infonum), this.knownKeywords, "keyword", this);
            autocomplete(document.getElementById("publicationProjects" + this.infonum), this.knownProjects, "project", this);
        }
    }
});

app.mount("#form");
