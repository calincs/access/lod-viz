//Check for valid url string
function validURL(str) {
    const matchpattern = /^https?:\/\/(?:www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)$/gm;
    return matchpattern.test(_string);
}

const app = Vue.createApp({});

app.component('form-info', {
    template:
    /*html*/
    `
    <div class="formContainer">
        <div class='formInfo' v-if="hidden" v-on:click="toggleDropdown()">
            <h3 class='shortLabel'>Name: {{ firstName }} {{lastName}}</h3>
            <h3 class='shortLabel'><a target='_blank' :href='orcidURL'>Orcid URL</a></h3> 
            <h3 class='shortLabel'><a target='_blank' :href='vizLink'>Website</a></h3> 
        </div>
        <div v-if="!hidden" class='hiddenFormInfo'>
            <form @submit="determineAction" autocomplete="off" onkeydown="return event.key != 'Enter';" method="post" action="/admin.php">
                <input type="hidden" :value="formAction" name="action"/>
                <input type="hidden" :value="id" name="id"/>
            
                <div id="hiddenForm-title">
                    <h2 class="label" style="margin-bottom: 15px;">Personal Information</h2>
                    <input type="button" value="Exit" v-on:click="toggleDropdown()"/>
                </div>

                <h3 class="label" style="margin-top: 0px;">First Name</h3>
                <input type="text" v-model="firstName" name="firstName"/>

                <h3 class="label">Last Name</h3>
                <input type="text" v-model="lastName" name="lastName"/>

                <h3 class="label">Alternate Names</h3>
                <input type="text" v-model="altNames" name="altNames"/>

                <h3 class="label">Orcid URL</h3>
                <div class="input-side">
                    <input type="text" v-model="orcidURL" style="width: 74%;" name="orcidURL"/>
                    <input type="button" value="View" class="beside-input" onclick="window.open(this.previousSibling.value, '_blank');"/>
                </div>

                <h3 class="label">Website</h3>
                <div class="input-side">
                    <input type="text" v-model="vizLink" style="width: 74%;" name="vizLink"/>
                    <input type="button" value="View" class="beside-input" onclick="window.open(this.previousSibling.value, '_blank');"/>
                </div>

                <br style="padding: 3px;">

                <event-info v-for="(event, index) in events" :key="index" :info="event"/>
                <participation-info v-for="(participation, index) in participations" :key="index" :info="participation"/>
                <publication-info v-for="(publication, index) in publications" :key="index" :info="publication"/>

                <br style="padding: 3px;">
                <input type="submit" value="Accept" v-on:click="formAction = 'accept'" style="cursor: pointer; margin-right: 10px"/>
                <input type="submit" value="Deny" v-on:click="formAction = 'deny'" style="cursor: pointer;"/>
                <br style="padding: 3px;">
            </form>
        </div>
    </div>
    `,



    data() {
        return {
            hidden: true,
            firstName: "",
            lastName: "",
            orcidURL: "",
            vizLink: "",
            altNames: "",
            events: [],
            participations: [],
            publications: [],
            formAction: "deny"
        }
    },



    props: {
        info: {
            type: String,
            required: true
        },
        id: {
            type: String,
            required: true
        }
    },



    methods: {
        toggleDropdown() { //Toggle minimized and maximized form views
            this.hidden = !this.hidden;
        },
        determineAction: function(e) { //Confirm accept/deny action
            if (confirm("Are you sure you want to " + this.formAction + "?")) {
                return true;
            }
            e.preventDefault();
            return false;
        }
    },



    mounted: async function() { //Parse input json string to populate form template
        var jsonInfo = JSON.parse(this.info);

        this.firstName = jsonInfo['firstName'];
        this.lastName = jsonInfo['lastName'];
        this.altNames = jsonInfo['altNames'];
        this.orcidURL = jsonInfo['orcidURL'];
        this.vizLink = jsonInfo['vizLink'];

        var type = "event";
        for (var i = 0; i < 2; i++) {
            if (jsonInfo[type + 'Name'] != null) {
                var unknownCount = 0;
                var keyCount = 0;
                var orgCount = 0;
                var orgRoleCount = 0;
                for (var count = 0; count < jsonInfo[type + 'Name'].length; count ++) {
                    var info = {};
                    info["name"] = jsonInfo[type + 'Name'][count];
                    info["role"] = jsonInfo[type + 'Role'][count];

                    info["type"] = jsonInfo[type + 'Type'][count];
                    if (jsonInfo[type + 'Type'][count] == "unknown") {
                        info["desc"] = jsonInfo[type + 'Desc'][unknownCount];

                        info["keywords"] = [];
                        keyCount += 1;
                        while (jsonInfo[type + 'Keywords'][keyCount] != null && jsonInfo[type + 'Keywords'][keyCount] != "") {
                            info["keywords"].push(jsonInfo[type + 'Keywords'][keyCount]);
                            keyCount += 1;
                        }

                        info["firstYear"] = jsonInfo[type + 'FirstYear'][unknownCount];
                        info["lastYear"] = jsonInfo[type + 'LastYear'][unknownCount];
                        info["website"] = jsonInfo[type + 'Website'][unknownCount];
                        info["image"] = jsonInfo[type + 'Image'][unknownCount];

                        info["orgs"] = [];
                        orgCount += 1;
                        while (jsonInfo[type + 'Projects'][orgCount] != null && jsonInfo[type + 'Projects'][orgCount] != "") {
                            var project = {};
                            project["name"] = jsonInfo[type + 'Projects'][orgCount];
                            project["role"] = jsonInfo[type + 'ProjectsRole'][orgRoleCount];
                            info["orgs"].push(project);
                            orgRoleCount += 1;
                            orgCount += 1;
                        }

                        info["extraInfo"] = jsonInfo[type + 'ExtraInfo'][unknownCount];

                        unknownCount += 1;
                    }

                    if (type == "event") {
                        this.events.push(info);
                    } else {
                        this.participations.push(info);
                    }
                }
            }
            type = "participation";
        }

        if (jsonInfo['publicationName'] != null) {
            var unknownCount = 0;
            var keyCount = 0;
            var orgCount = 0;
            var orgRoleCount = 0;
            for (var count = 0; count < jsonInfo['publicationName'].length; count ++) {
                var publication = {};
                publication["name"] = jsonInfo['publicationName'][count];
                publication["role"] = jsonInfo['publicationRole'][count];

                publication["type"] = jsonInfo['publicationType'][count];
                if (jsonInfo['publicationType'][count] == "unknown") {
                    publication["doi"] = jsonInfo['publicationDoi'][unknownCount];
                    
                    publication["keywords"] = [];
                    keyCount += 1;
                    while (jsonInfo['publicationKeywords'][keyCount] != null && jsonInfo['publicationKeywords'][keyCount] != "") {
                        publication["keywords"].push(jsonInfo['publicationKeywords'][keyCount]);
                        keyCount += 1;
                    }

                    publication["orgs"] = [];
                    orgCount += 1;
                    while (jsonInfo['publicationProjects'][orgCount] != null && jsonInfo['publicationProjects'][orgCount] != "") {
                        var project = {};
                        project["name"] = jsonInfo['publicationProjects'][orgCount];
                        project["role"] = jsonInfo['publicationProjectsRole'][orgRoleCount];
                        publication["orgs"].push(project);
                        orgRoleCount += 1;
                        orgCount += 1;
                    }

                    publication["authors"] = jsonInfo['publicationAuthors'][unknownCount];
                    publication["date"] = jsonInfo['publicationDate'][unknownCount];
                    publication["year"] = jsonInfo['publicationYear'][unknownCount];
                    publication["link"] = jsonInfo['publicationLink'][unknownCount];
                    publication["reference"] = jsonInfo['publicationReference'][unknownCount];
                    publication["extraInfo"] = jsonInfo['publicationExtraInfo'][unknownCount];

                    unknownCount += 1;
                }

                this.publications.push(publication);
            }
        }
    }
});

app.component("event-info", { //Event Form
    template:
    /*html*/
    `
    <div class="infoCard">
        <h2 class="label" style="padding-top: 15px; margin-bottom: 15px; margin-top: 15px;">Event</h2>
        <div class="input-side" :style="spacing">
            <input type="text" v-model="info['name']" style="width: 74%;" name="eventName[]"/>
            <select class="beside-input" v-model="info['role']" name="eventRole[]">
                <option>Organizer</option>
                <option>Participant</option>
                <option>Attendee</option>
                <option>Other</option>
            </select>
        </div>

        <input type="hidden" name="eventType[]" v-model="info['type']" />
        <div v-if="info['type'] == 'unknown'">
            <h3 class="label">Description</h3>
            <input type="text" v-model="info['desc']" name="eventDesc[]"/>

            <h3 class="label">Keywords</h3>
            <input type="hidden" value="" name="eventKeywords[]"/>
            <input type="text" v-for="keyword in info['keywords']" v-model="keyword" name="eventKeywords[]"/>

            <h3 class="label">First Year Held</h3>
            <input type="text" v-model="info['firstYear']" name="eventFirstYear[]"/>

            <h3 class="label">Last Year Held</h3>
            <input type="text" v-model="info['lastYear']" name="eventLastYear[]"/>

            <h3 class="label">Website</h3>
            <div class="input-side">
                <input type="text" v-model="info['website']" style="width: 74%;" name="eventWebsite[]"/>
                <input type="button" value="View" class="beside-input" onclick="window.open(this.previousSibling.value, '_blank');"/>
            </div>

            <h3 class="label">Image</h3>
            <div class="input-side">
                <input type="text" v-model="info['image']" style="width: 74%;" name="eventImage[]"/>
                <input type="button" value="View" class="beside-input" v-on:click="viewImage(info['image'])"/>
            </div>

            <h3 class="label">Related Organizations/Projects</h3>
            <input type="hidden" value="" name="eventProjects[]"/>
            <div class="input-side" v-for="org in info['orgs']">
                <input type="text" v-model="org.name" style="width: 74%;" name="eventProjects[]"/>
                <select class="beside-input" v-model="org.role" name="eventProjectsRole[]">
                    <option>Sponsor</option>
                    <option>Host</option>
                    <option>Other</option>
                </select>
            </div>

            <h3 class="label">Additional Information</h3>
            <input style="margin-bottom: 15px;" type="text" v-model="info['extraInfo']" name="eventExtraInfo[]"/>
        </div>
    </div>
    <br style="padding: 3px;">
    `,


    props: {
        info: {
            type: Object,
            required: true
        }
    },



    computed: {
        spacing() {
            if (this.info['desc'] == null) {
                return "margin-bottom: 15px;";
            } else {
                return "";
            }
        }
    },



    methods: {
        viewImage(value) { //Display preview image
            if (validURL(value)) {
                console.log("NOT HERE");
                window.open(value, '_blank');
            } else {
                console.log("HERE");
                window.open("/data/formUploads/" + value, '_blank');
            }
        }
    }
});

app.component("participation-info", { //Participation form
    template:
    /*html*/
    `
    <div class="infoCard">
        <h2 class="label" style="padding-top: 15px; margin-bottom: 15px; margin-top: 15px;">Organization/Project</h2>
        <div class="input-side" :style="spacing">
            <input type="text" v-model="info['name']" style="width: 74%;" name="participationName[]"/>
            <select class="beside-input" v-model="info['role']" name="participationRole[]">
                <option>Principal Investigator</option>
                <option>Co-Investigator</option>
                <option>Collaborator</option>
                <option>Member</option>
                <option>RA</option>
                <option>Postdoc</option>
            </select>
        </div>

        <input type="hidden" name="participationType[]" v-model="info['type']" />
        <div v-if="info['type'] == 'unknown'">
            <h3 class="label">Description</h3>
            <input type="text" v-model="info['desc']" name="participationDesc[]"/>

            <h3 class="label">Keywords</h3>
            <input type="hidden" value="" name="participationKeywords[]"/>
            <input type="text" v-for="keyword in info['keywords']" v-model="keyword" name="participationKeywords[]"/>

            <h3 class="label">Start Date</h3>
            <input type="text" v-model="info['firstYear']" name="participationFirstYear[]"/>

            <h3 class="label">End Date</h3>
            <input type="text" v-model="info['lastYear']" name="participationLastYear[]"/>

            <h3 class="label">Website</h3>
            <div class="input-side">
                <input type="text" v-model="info['website']" style="width: 74%;" name="participationWebsite[]"/>
                <input type="button" value="View" class="beside-input" onclick="window.open(this.previousSibling.value, '_blank');"/>
            </div>

            <h3 class="label">Image</h3>
            <div class="input-side">
                <input type="text" v-model="info['image']" style="width: 74%;" name="participationImage[]"/>
                <input type="button" value="View" class="beside-input" v-on:click="viewImage(info['image'])"/>
            </div>

            <h3 class="label">Related Organizations/Projects</h3>
            <input type="hidden" value="" name="participationProjects[]"/>
            <div class="input-side" v-for="org in info['orgs']">
                <input type="text" v-model="org.name" style="width: 74%;" name="participationProjects[]"/>
                <select class="beside-input" v-model="org.role" name="participationProjectsRole[]">
                    <option>Sponsor</option>
                    <option>Host</option>
                    <option>Other</option>
                </select>
            </div>

            <h3 class="label">Additional Information</h3>
            <input style="margin-bottom: 15px;" type="text" v-model="info['extraInfo']" name="participationExtraInfo[]"/>
        </div>
    </div>
    <br style="padding: 3px;">
    `,


    props: {
        info: {
            type: Object,
            required: true
        }
    },



    computed: {
        spacing() {
            if (this.info['desc'] == null) {
                return "margin-bottom: 15px;";
            } else {
                return "";
            }
        }
    },



    methods: {
        viewImage(value) { //Display preview image
            if (validURL(value)) {
                console.log("NOT HERE");
                window.open(value, '_blank');
            } else {
                console.log("HERE");
                window.open("/data/formUploads/" + value, '_blank');
            }
        }
    }
});

app.component("publication-info", { //Publication form
    template:
    /*html*/
    `
    <div class="infoCard">
        <h2 class="label" style="padding-top: 15px; margin-bottom: 15px; margin-top: 15px;">Publication</h2>
        <div class="input-side" :style="spacing">
            <input type="text" v-model="info['name']" style="width: 74%;" name="publicationName[]"/>
            <select class="beside-input" v-model="info['role']" name="publicationRole[]">
                <option>Author</option>
                <option>Editor</option>
                <option>Other</option>
            </select>
        </div>

        <div v-if="info['type'] == 'unknown'">
            <h3 class="label">DOI</h3>
            <input type="text" v-model="info['doi']" name="publicationDoi[]"/>

            <h3 class="label">Keywords</h3>
            <input type="hidden" value="" name="publicationKeywords[]"/>
            <input type="text" v-for="keyword in info['keywords']" v-model="keyword" name="publicationKeywords[]"/>

            <h3 class="label">Related Organizations/Projects</h3>
            <input type="hidden" value="" name="publicationProjects[]"/>
            <div class="input-side" v-for="org in info['orgs']">
                <input type="text" v-model="org.name" style="width: 74%;" name="publicationProjects[]"/>
                <select class="beside-input" v-model="org.role" name="publicationProjectsRole[]">
                    <option>Sponsor</option>
                    <option>Institutional Affiliation</option>
                    <option>Research Partner</option>
                    <option>Publishing/Dissemination Partner</option>
                </select>
            </div>

            <h3 class="label">Authors</h3>
            <input type="text" v-model="info['authors']" name="publicationAuthors[]"/>

            <h3 class="label">Date</h3>
            <input type="text" v-model="info['date']" name="publicationDate[]"/>

            <h3 class="label">Year</h3>
            <input type="text" v-model="info['year']" name="publicationYear[]"/>

            <h3 class="label">Link</h3>
            <div class="input-side">
                <input type="text" v-model="info['link']" style="width: 74%;" name="publicationLink[]"/>
                <input type="button" value="View" class="beside-input" onclick="window.open(this.previousSibling.value, '_blank');"/>
            </div>

            <h3 class="label">Reference</h3>
            <input type="text" v-model="info['reference']" name="publicationReference[]"/>

            <h3 class="label">Additional Information</h3>
            <input style="margin-bottom: 15px;" type="text" v-model="info['extraInfo']" name="publicationExtraInfo[]"/>
        </div>
    </div>
    <br style="padding: 3px;">
    `,


    props: {
        info: {
            type: Object,
            required: true
        }
    }
});

app.mount("#forms");
